import Vue from 'vue'
import Router from 'vue-router'
import list from './components/list.vue'
import about from './components/about.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'list',
      component: list
    },
    {
      path: '/about',
      name: 'about',
      component: about
    }
  ]
})
