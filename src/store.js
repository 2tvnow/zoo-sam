import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    animal: [
      { type: 'lion', sex: 'Male', age: 5 },
      { type: 'elephant', sex: 'Female', age: 3 },
      { type: 'eagle', sex: 'Male', age: 4 },
      { type: 'cat', sex: 'Female', age: 9 },
      { type: 'dog', sex: 'Male', age: 2 }
    ]
  },
  mutations: {
    ADD: function (state, item) {
      state.animal.push(item)
    },
    EDIT: function (state, item) {
      Object.assign(state.animal[item.index], item.item)
    },
    DELETE: function (state, index) {
      state.animal.splice(index, 1)
    }
  },
  actions: {
    add: function ({ commit }, item) {
      commit('ADD', item)
    },
    edit: function ({ commit }, item) {
      commit('EDIT', item)
    },
    delete: function ({ commit }, index) {
      commit('DELETE', index)
    }
  },
  getters: {
    getAnimal: function (state) {
      return state.animal
    }
  }
})
